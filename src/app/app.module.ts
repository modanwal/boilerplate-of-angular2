import { BrowserModule } from '@angular/platform-browser';
import {ModuleWithProviders, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {AppRootModule} from './app-root/app-root.module';
import {RouterModule} from "@angular/router";

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
], { useHash: false });

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, AppRootModule, RouterModule, rootRouting
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
