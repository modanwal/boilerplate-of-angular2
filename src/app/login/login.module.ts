
import {NgModule} from '@angular/core';
import {LoginComponent} from './login.component';
import {SharedModule} from '../shared/shared.module';

export const loginRouting = [
  {
    path: 'login',
    component: LoginComponent
  }
]
@NgModule({
  imports: [SharedModule],
  declarations: [LoginComponent],
  providers: [],
  exports: []
})
export class LoginModule {}
