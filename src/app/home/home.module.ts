import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';

 export  const homeRouting = [
    {
      path: '',
      component : HomeComponent
    }
  ];
@NgModule({
  imports : [],
  declarations : [HomeComponent],
  exports : []
})
export class HomeModule {}
