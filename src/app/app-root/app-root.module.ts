

import {RouterModule} from '@angular/router';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {HomeModule, homeRouting} from '../home/home.module';
import {AppRootComponent} from './app-root.component';
import {LayoutComponent} from '../shared/layout/layout.component';
import {LoginModule, loginRouting} from '../login/login.module';


export const appRootRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    component : AppRootComponent,
    children : [
       ...homeRouting,
       ...loginRouting
    ]
  }
]);
@NgModule({
  imports: [SharedModule, appRootRouting, HomeModule, LoginModule],
  declarations: [AppRootComponent, LayoutComponent],
  entryComponents: [],
  providers: [],
  exports: []
})
export class AppRootModule {}
