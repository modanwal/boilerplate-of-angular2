import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class JwtService {
  public isBrowser: boolean = isPlatformBrowser(this.platformId);
  constructor(@Inject(PLATFORM_ID) private platformId){

  }

  getToken(): String {
    if (this.isBrowser){
      return window.localStorage['jwtToken'];
    }
  }

  saveToken(token: String) {
    if (this.isBrowser){
      window.localStorage['jwtToken'] = token;
    }
  }

  destroyToken() {
    if(this.isBrowser){
      window.localStorage.removeItem('jwtToken');
    }
  }

}
