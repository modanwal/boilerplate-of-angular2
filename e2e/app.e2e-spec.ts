import { MysuperadminPage } from './app.po';

describe('mysuperadmin App', () => {
  let page: MysuperadminPage;

  beforeEach(() => {
    page = new MysuperadminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
